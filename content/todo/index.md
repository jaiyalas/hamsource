+++
title = "TODO"
date = "2024-04-09"
displayInMenu = true
displayInList = false
dropCap = false
toc = true
+++

# 緣起

「雲泥之間」是一系列關於 Haskell 的教學/筆記文章。


最初的目的只是希望釐清幾個常見但是容易混淆的詞彙諸如 boxed 和 lifted 等等，
但是，隨著深入這些詞彙背後的情境，問題漸漸的形成了一個共同的問題：
「**我們怎麼**(在 Haskell 中)**描述資訊**(information[^1])**？**」。
為了進一步地理解這個問題，便產生了這個系列文章。
也許最終沒有辦法有個結論或是答案，但是至少這系列的文章可以留下一些線索，
讓我們可以朝著答案前進。

# 系列結構與列表

我們更進一步地把問題粗略地分成三個不同的層次：

1. 要怎麼在計算世界裡面表示資訊？
2. 在 Haskell 的世界中資訊被怎麼表示？
3. Haskell 怎麼表示資訊？

而這三個不同的問題有著各自專屬的理論領域以及著重的重點。
接著，我們根據這三個層次來將此系列文章分成三個子系列：
因為我們探討的是如何在程式(或計算)中表示資訊，
所以第一個子系列，「Denotational Semantics and Domain」，
將試著整理程式語意以及相關的數學背景。
一旦對於抽象資料建構有一定程度的理解了，
我們接著會在第二個子系列「Constructions on Datatype in Haskell」
試著歸納出 Haskell 中怎麼建構或是表示資訊的方法。
以及這些方法是怎麼對應到數學理論上面的。
最後，最終的子系列「Haskell, GHC and STG Machine」
則是嘗試理解 Haskell 底層怎麼提供 Haskell 能力去讓他可以如是表示資訊。
或者說，試圖去理解 Haskell 底層是怎麼實踐那些抽象的資訊表示方法的。

## Denotational Semantics and Domain

+ [Pointed? Lifted!](/content/posts/2016-04-13-liftpoint.html)
+ [(尚未完成) Constructions on Datatype](#)
+ [(尚未完成) Eval Strategy and Bottom](#)
+ [(尚未完成) Algebraic Datatype](#)

## Constructions on Datatype in Haskell

+ [(尚未完成) Kinds, `#` and `*`](#)
+ [(尚未完成) Lazeness](#)

## Haskell, GHC and STG Machine

+ [(尚未完成) something you must know about STG Machine](#)
+ [(尚未完成) Heap objects in STG Machine](#)
+ [(尚未完成) Boxed/unboxed](#)
+ [(尚未完成) Pack/unpack](#)
+ [(尚未完成) Closure/thunk](#)

# 參考資料

## Stage I

* Denotational Semantics
    + Hutton's [Introduction to Domain Theory](http://www.cs.nott.ac.uk/~pszgmh/domains.html)
    + Horwitz's [Domain Theory](http://pages.cs.wisc.edu/~horwitz/CS704-NOTES/8.DOMAIN-THEORY.html)
    + Haskell Wikibook [Denotational semantics]https://en.wikibooks.org/wiki/Haskell/Denotational_semantics#Algebraic_Data_Types
* Layzeness
    + Yang's [How I Learned to Stop Worrying and Love the ⊥](http://blog.ezyang.com/2010/12/how-i-learned-to-stop-worrying-and-love-the-bottom/)
    + Yang's[Hussling Haskell types into Hasse diagrams](http://blog.ezyang.com/2010/12/hussling-haskell-types-into-hasse-diagrams/)
    + Yang's [Gin and monotonic](http://blog.ezyang.com/2010/12/gin-and-monotonic/) ([Errata](http://blog.ezyang.com/2010/12/errata-for-gin-and-monotonic/))
    + Yang's [ω: I’m lubbin’ it](http://blog.ezyang.com/2010/12/omega-i-m-lubbin-it/) -- a great way to modularize laziness
    + Harper's [The Point of Laziness](https://existentialtype.wordpress.com/2011/04/24/the-real-point-of-laziness/)
* ADT
    + Wikipedia [Algebraic data type](https://en.wikipedia.org/wiki/Algebraic_data_type)
    + Wikipedia [Generalized algebraic data type](https://en.wikipedia.org/wiki/Generalized_algebraic_data_type)
    + Haskell wiki [Algebraic data type](https://wiki.haskell.org/Algebraic_data_type)
    + Haskell wiki [GADT](https://wiki.haskell.org/GADT)

## Stage II

* WHNF
    + Haskell wiki [weak head normal form](https://wiki.haskell.org/Weak_head_normal_form)
    + Haskell wikibook [weak head normal form](https://en.wikibooks.org/wiki/Haskell/Graph_reduction#Weak_Head_Normal_Form)
* Eval and Buttom
    + Apfelmus' [Haskell’s Non-Strict Semantics](https://hackhands.com/non-strict-semantics-haskell/)
* lifting(in general)
    + haskell wiki [Lifting](https://wiki.haskell.org/Lifting)
* kinds and datatype
    + GHC wiki [Kinds](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/Kinds)
    + GHC wiki [Unlifted data types](https://ghc.haskell.org/trac/ghc/wiki/UnliftedDataTypes)
    + GHC wiki [The data type Type and its friends](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/TypeType)

## Stage III

* unpack
    + GHC user's guide [7.22.11. UNPACK pragma](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/pragmas.html#unpack-pragma)
    + Haskell wiki [Performance/Data types](https://wiki.haskell.org/Performance/Data_types)
    + GHC wiki [Unpacked sum types](https://ghc.haskell.org/trac/ghc/wiki/UnpackedSumTypes)
* unboxed
    + GHC user's guide [7.2. Unboxed types and primitive operations](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/primitives.html)
    + Hashell wiki [Unboxed type](https://wiki.haskell.org/Unboxed_type)
    + Haskell-Cafe [Boxed versus Unboxed](https://mail.haskell.org/pipermail/haskell-cafe/2002-October/003433.html)
* Thunk
    + stack overflow [How much memory does a thunk use?](http://stackoverflow.com/questions/13982863/how-much-memory-does-a-thunk-use)
* STG
    + GHC wiki [I know kung fu: learning STG by example](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/GeneratedCode)
    + GHC wiki [GHC Commentary: The Layout of Heap Objects](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Rts/Storage/HeapObjects)

## 其他或是無關的參考資料與註記

Text and how to pick a text library

* Yang's [How to pick your string library in Haskell](http://blog.ezyang.com/2010/08/strings-in-haskell/)
* Shmalko's [Haskell String Types](http://www.alexeyshmalko.com/2015/haskell-string-types/)
* Haskell wiki [Dealing with binary data](https://wiki.haskell.org/Dealing_with_binary_data)

[^1]: 這裡所謂的資訊 (information) 是泛指包含廣泛的資料 (data) 或是資料結構 (data structure) 等等。

# 有空翻翻

## 工具或理論

### Euterpea

+ [Euterpea](https://hackage.haskell.org/package/Euterpea) - eDSL for computer music development. [website](http://www.euterpea.com/)

### Lens

+ [Program imperatively using Haskell lenses](http://www.haskellforall.com/2013/05/program-imperatively-using-haskell.html)

### Coerce, Roles and newtype

+ [Adding safe coercions to Haskell (Breitner)](https://www.joachim-breitner.de/blog/610-Adding_safe_coercions_to_Haskell)
+ [Roles Overview (GHC Developer Wiki)](https://ghc.haskell.org/trac/ghc/wiki/SafeRoles/RolesOverview)
+ [Roles (GHC Developer Wiki)](https://ghc.haskell.org/trac/ghc/wiki/Roles)
+ [Roles, Abstraction & Safety (GHC Developer Wiki)](https://ghc.haskell.org/trac/ghc/wiki/SafeRoles)
+ [GHC/Coerce (Haskell Wiki)](https://wiki.haskell.org/GHC/Coercible)
+ [Newtype (Haskell Wiki)](https://wiki.haskell.org/Newtype)
+ [9.36. Roles (GHC User Guide)](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#roles)

### others

+ [Why free monads matter](http://www.haskellforall.com/2012/06/you-could-have-invented-free-monads.html)
+ [GHC/Type Families (Haskell Wiki)](https://wiki.haskell.org/GHC/Type_families)

## 影片

+ [**The Essence of FRP** by Conal Elliott](https://begriffs.com/posts/2015-07-22-essence-of-frp.html)
+ [**Adventure with Types in Haskell** by Simon Peyton Jones](https://www.youtube.com/playlist?list=PLWXgJA_3LGsk18ww3jfeZjGlxEIGo8cGN)
+ [**A Sensible Intro to FRP** by Tikhon Jelvis](https://begriffs.com/posts/2016-07-27-tikhon-on-frp.html)
+ [**What Code Does vs What Code Means** by John Wiegley](https://begriffs.com/posts/2015-12-26-what-code-means.html)

# Links

## [GHC Users Guide](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html)

+ [9. GHC Language Features](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/lang.html)
    + [9.6. Extensions to the “deriving” mechanism](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#extensions-to-the-deriving-mechanism)
        + [9.6.9. Generalised derived instances for newtypes](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#generalised-derived-instances-for-newtypes)
    + [9.9. Type families](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#type-families)
    + [9.10. Datatype promotion](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#datatype-promotion)
    + [9.36. Roles (GHC User Guide)](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html#roles)

## [Haskell 2010 Report](https://www.haskell.org/onlinereport/haskell2010/)  

+ [Contents](https://www.haskell.org/onlinereport/haskell2010/haskellli1.html)
+ [3 Expressions](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-220003)
    + [3.12 Let Expressions](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-440003.12)
    + [3.13 Case Expressions](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-460003.13)
    + [3.17 Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-580003.17)
        + [3.17.3 Formal Semantics of Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-610003.17.3)
+ [4 Declarations and Bindings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-620004)
    + [4.2 User-Defined Datatypes](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-680004.2)
        + [4.2.1 Algebraic Datatype Declarations](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-690004.2.1)
        + [4.2.3 Datatype Renamings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-740004.2.3)
    + [4.3 Type Classes and Overloading](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-750004.3)
    + [4.4 Nested Declarations](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-800004.4)
        + [4.4.3 Function and Pattern Bindings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-830004.4.3)
    + [4.5 Static Semantics of Function and Pattern Bindings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-880004.5)
+ [6 Predefined Types and Classes](https://www.haskell.org/onlinereport/haskell2010/haskellch6.html#x13-1160006)
    + [6.2 Strict Evaluation](https://www.haskell.org/onlinereport/haskell2010/haskellch6.html#x13-1260006.2)
    + [6.4 Numbers](https://www.haskell.org/onlinereport/haskell2010/haskellch6.html#x13-1350006.4)

# 雜記/待看

+ [records](http://wiki.portal.chalmers.se/agda/pmwiki.php?n=Docs.Records)

# Tutorials

+ [A Practical Agda Tutorial](http://people.inf.elte.hu/divip/AgdaTutorial/Index.html)
+ [Computer Aided Formal Reasoning](http://www.cs.nott.ac.uk/~psztxa/g53cfr/)
+ [Agda’s official documentation](http://agda.readthedocs.io/en/latest/)
