---
title: "Youmoutoohana"
date: 2019-12-03
description: "Another cozy coffee shop in Taipei"
categories: ["Life"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Another cozy coffee shop in Taipei.

![](2019-12-03_01.jpg)
![](2019-12-03_02.jpg)
![](2019-12-03_03.jpg)

You can study or work here. 文青爆炸！
