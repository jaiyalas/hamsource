---
title: "YongKang St."
date: 2019-09-12
description: "Enjoy to come to here for making tea, eating and relaxing."
categories: ["Life"]
dropCap: true
displayInMenu: false
displayInList: true
draft: false
---

Enjoy to come to YongKang St. for making tea, eating and relaxing. But I've never bought this famous street food until today.

![](2019-09-12.jpg)
