---
title: Basic image processing with JuicyPixels in Haskell
author: Jaiyalas
date: 2015-05-27
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

<p class="warn">
<i class="fa fa-exclamation-triangle fa-1x"></i> This is an unfinished Post <i class="fa fa-exclamation-triangle fa-1x "></i>
</p>

I will introduce some basic ways to read/write/manipulate image file with JuicyPixels.

<!--more-->

## Pixel!

### Byte-level Manipulation in Haskell

In haskell, usually we use [`Data.ByteString`](http://hackage.haskell.org/package/bytestring-0.10.6.0/docs/Data-ByteString.html) or [`Data.ByteString.Lazy`](http://hackage.haskell.org/package/bytestring-0.10.6.0/docs/Data-ByteString-Lazy.html) as a container of raw data. Which means we will use some thing like [`Word8`](http://hackage.haskell.org/package/base-4.8.0.0/docs/Data-Word.html#t:Word8) to present one byte and thus manipulate data byte by byte. 

Additionally, since `Word8` are an instance of type class `Num` and `Integral`, one could simply use 
```haskell
fromInteger :: Integer -> Word8  
toInteger :: Word8 -> Integer  
``` 
to handle bytes as integers. The module `Data.ByteString` and `Data.ByteString.Lazy` are both provide some functions to convert between `ByteString` and `[Word8]`.

### Present Pixels in JuicyPixels

Pixel 是一個 type class 內涵一些 operation 和一個 associated type synonym - PixelBaseComponent


Firstly JuicyPixels provides several basic pixel types,
```haskell
type Pixel8 = Word8
data PixelRGB8 = PixelRGB8 !Pixel8 !Pixel8 !Pixel8
data PixelRGBA8 = PixelRGBA8 !Pixel8 !Pixel8 !Pixel8 !Pixel8
data PixelCMYK8 = PixelCMYK8 !Pixel8 !Pixel8 !Pixel8 !Pixel8
```
However, this is 

A type `a` can be one kind of pixel if it: (1) is `Storable` into `Vector`; (2) is a number; (3) provides equality operator.

```haskell
class ( Storable (PixelBaseComponent a)
      , Num (PixelBaseComponent a)
      , Eq a ) => Pixel a where
    type PixelBaseComponent a :: *
    pixelOpacity :: a -> PixelBaseComponent a
    -- skipped
```

## Load image with JuicyPixels

In [JuicyPixels](http://hackage.haskell.org/package/JuicyPixels-3.2.5.1), ...


### Define `Image`

As its name implies, the `Image` contains information of width and height and a `Vector` of (*some kind of*) pixels. The detail of `Vector` is beyond this post, the only thing we should know is that `Vector` is just an efficient linear structure which will be used to preserve pixels. 

```haskell
data Image a = Image
    { imageWidth  :: {-# UNPACK #-} !Int
    , imageHeight :: {-# UNPACK #-} !Int
    , imageData   :: Vector (PixelBaseComponent a)
    }
```

PS. About that `{-# UNPACK #-}`, please read [Unpacking strict fields](https://wiki.haskell.org/Performance/Data_types#Unpacking_strict_fields)

### Load Image as `DynamicImage`

JuicyPixels provides functions to enable directly loading images or decoding loaded images, which should be `ByteString`, into haskell. Either way the result will be presented by a `DynamicImage` structure. As shown in following code, a `DynamicImage` includes an `Image` structure with corresponding pixel type. Please notice, here we only show the most common cases. 
```haskell
data DynamicImage = ImageY8 (Image Pixel8)	
                  | ImageRGB8 (Image PixelRGB8)
                  | ImageRGBA8 (Image PixelRGBA8)
                  | ImageCMYK8 (Image PixelCMYK8)
```
