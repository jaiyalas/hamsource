---
title: Free Functor
author: Jaiyalas
date: 2016-06-18
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Free Functor, Adjoint Functor and Structures.

<!--more-->

根本還沒寫...

+ [free structure @ haskell wiki](https://wiki.haskell.org/Free_structure)
+ [free functor @ nLab](https://ncatlab.org/nlab/show/free+functor)
+ [adjoint functor @ nLab](https://ncatlab.org/nlab/show/adjoint+functor)
+ [forgetful functor @ nLab](https://ncatlab.org/nlab/show/forgetful+functor)
+ [Stuff, structure, and properties @ nLab](https://ncatlab.org/nlab/show/stuff%2C+structure%2C+property)
+ [groupoid @ nLab](https://ncatlab.org/nlab/show/groupoid)
