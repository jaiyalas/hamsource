---
title: Families of Haskell
author: Jaiyalas
date: 2016-07-27
description: ""
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false

---

***Indexed*** **type family**，或簡稱為 type family，
是一種在 haskell 裡面實現 type-level programming 的機制。

使用 type family 我們可以在 type lvel 加入更多關於 datatype 的描述。

一方面我們可以用 type family 去對我們的 datatype 進行更精確的描述；或者，
也可以用來實現 ad-hoc polymorphism。

<!--more-->

<div class="block-notes">
要在 Haskell 中使用 type family 需要打開 `-XTypeFamilies`。
</div>

## standard derivable typeclasses

say, `Eq`, `Ord`, `Read`, `Show` 可以 derive 出來
但是 `Enum` 和 `Num` 這些就推不出來

### pre-GHC-7.6

Module boundary control — Haskell code compiled using the safe language is guaranteed to only access symbols that are publicly available to it through other modules export lists. An important part of this is that safe compiled code is not able to examine or create data values using data constructors that it cannot import. If a module M establishes some invariants through careful use of its export list then code compiled using the safe language that imports M is guaranteed to respect those invariants. Because of this, Template Haskell and GeneralizedNewtypeDeriving are disabled in the safe language as they can be used to violate this property.

## Roles

roles 可以被明確地宣告出來：`-XRoleAnnotations`

為了描述辨別兩個 datatypes 是不是一樣的 (having the same underlying representation)，
我們引入 roles 來描述並區別 type constructors 的參數 (type parameter)。

+ If a type parameter has a *phantom* role, then we need no further information.
+ If a type parameter has a *representational* role, then the two types must have the same representation.
    + (If T‘s first parameter’s role is representational, then T Age Bool c and T Int Bool c would have the same representation, because Age and Int have the same representation.)
+ If a type parameter has a *nominal* role, then the two types that differ must not actually differ at all
    + they must be identical (after type family reduction).


+ (->) has two *representational* parameters
+ (~) has two *nominal* parameters;
+ all type families’ parameters are *nominal*;
+ all GADT-like parameters are *nominal*.

+ the default role for datatypes and synonyms is *phantom*;
+ the default role for classes is *nominal*.

+ for datatypes and synonyms, any parameters unused in the right-hand side
(or used only in other types in phantom positions) will be *phantom*.
+ Role annotation wouldn't be allowed for type synonyms

```{.haskell}
newtype Alpha = Alpha ()
newtype Beta = Beta ()
```

### Nominal(名義上)

+ `a` has nominal role: 經過 type family reduction 之後兩個 type 真的會是同一個。
+ `T a = C a`: `a` 有用到，而且是直接拿來用
+ 給定兩個一樣 representation 的 type parameter 會得到一樣 representation datatype
+ i.e. `T Alpha = T Beta`

### Representational()

+ `a` has representational role: 兩個 type 具有同一個 representation。
+ `T a = C (F a)`: `a` 有用到，但是不是直接用
+ 給定兩個一樣 representation 的 type parameter 不一定會得到一樣的 datatype
+ i.e. `T Alpha \= T Beta` (depends on what `F` is)

### Phantom(幻影)

+ `a` has phantom role: nothing interesting。
+ `T a = C`: `a` 根本沒用到
+ 就算給兩個不一樣 representations 還是會得到一樣的 datatype

## Coercible

If datatypes `a` and `b` have the same underlying representation,
the typeclass `Coercible a b => a -> b` ensures and provides the way to converse a `a` into `b`.


`Data.Coerce` 提供了:
1. compile-time 檢查兩個 datatypes 是不是一樣的機制。
2. 順便產生一個 casting function 給你用 (而且因為 ghc 知道兩個 types 是一樣的，所以底下就直接當做一樣的這樣作弊過去而不會真的算它)

### Motivative Problem

```{.haskell}


```
