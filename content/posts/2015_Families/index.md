---
title: Type Families and Associated Types.
author: Jaiyalas
date: 2015-05-27
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Introducing some basic concepts and applications of Type Families and Associated Types.

<!--more-->

## Necessary Pragmas

### Type Classes with Multiple Parameters


```haskell
{-# LANGUAGE MultiParamTypeClasses #-}
```

The typeclass restrict itself single parameter only. MultiParamTypeClasses can release such limitation.

    class TC a b where
      foo :: a -> b -> a

### Repeated Type Variables in Type Instances

```haskell
{-# LANGUAGE FlexibleInstances #-}
```

For any instance of typeclass, haskell assume that each type variable appears at most once in the instance by default. This restriction can be released by simply using `FlexibleInstances`.

    class TC a b where
      foo a -> b -> a
    instance TC a a where
      foo _ = id

Notice that there is a quite big chance that `FlexibleInstances` and `FunctionalDependencies` are used together.

    class TC a b | a -> b where
      foo a -> b  
    instance TC (a,b) a where
      foo (x,y) = x

## Associated Type

### Functional Dependency

```haskell
{-# LANGUAGE FunctionalDependencies #-}
```

Sometimes a multi-parameter typeclass will be over-generalized to use. Especially when there is some relationship between those parameters. With functional dependency one can define some restriction required.

For example, for typeclass as follows

    class TC a b where
      foo :: a -> b

one might want to restrict that `b` must not be any arbitrary type and shomehow related to `a`. In that case, one can define with functional dependency:

    class TC a b | a -> b where
      foo :: a -> b

Now the second parameter of an instance of `TC` must contains no variables excepting variables introduced in the first parameter. Both of following instance will be legal.

    instance TC (x, Int) -> x where
      foo = undefined
    instance TC (x, Int) -> Int where
      foo = undefined

## Type Family

...
