---
title: Working with Contravariant Functors.
author: Jaiyalas
date: 2015-08-03
categories: ["Category"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

In category, a contravariant functor is not thing but a functor with reversed output. For example, given type `a`, `b` in category `C` and a morphism `f :: a -> b`, a functor `CF` is a contravariant functor if `CF f` has type as `CF b -> CF a`

<!--more-->

```haskell
Data.Functor.Contravariant
```

```haskell
class Contravariant f where
  contramap :: (a -> b) -> f b -> f a
  (>$) :: b -> f b -> f a
  (>$) = contramap . const
```
