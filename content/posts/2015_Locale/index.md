---
title: Localization with Data.Time.Format.Locale.
author: Jaiyalas
date: 2015-05-29
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

In haskell, locale is defined as a set of *how to present date and time nicely*. Here we only discuss `Data.Time.Format.TimeLocale` not `Ststem.Locale`.

<!--more-->

Module `Data.Time.Format` provides 

```haskell
data TimeLocale = TimeLocale {
        wDays  :: [(String, String)],
        months :: [(String, String)],
        amPm   :: (String, String),
        dateTimeFmt, dateFmt,
        timeFmt, time12Fmt :: String,
        knownTimeZones :: [TimeZone]
        } deriving (Eq, Ord, Show)
```

to allow programmer to define a presenting format for date and time. As well as, it also defines a default locale for the commonest American style.

```haskell
defaultTimeLocale :: TimeLocale
defaultTimeLocale =  TimeLocale {
        wDays  = [("Sunday",   "Sun"),  ("Monday",    "Mon"),
                  ("Tuesday",  "Tue"),  ("Wednesday", "Wed"),
                  ("Thursday", "Thu"),  ("Friday",    "Fri"),
                  ("Saturday", "Sat")],
        months = [("January",   "Jan"), ("February",  "Feb"),
                  ("March",     "Mar"), ("April",     "Apr"),
                  ("May",       "May"), ("June",      "Jun"),
                  ("July",      "Jul"), ("August",    "Aug"),
                  ("September", "Sep"), ("October",   "Oct"),
                  ("November",  "Nov"), ("December",  "Dec")],
        amPm = ("AM", "PM"),
        dateTimeFmt = "%a %b %e %H:%M:%S %Z %Y",
        dateFmt = "%m/%d/%y",
        timeFmt = "%H:%M:%S",
        time12Fmt = "%I:%M:%S %p",
        knownTimeZones =
            [
            TimeZone 0 False "UT",
            TimeZone 0 False "GMT",
            TimeZone (-5 * 60) False "EST",
            TimeZone (-4 * 60) True "EDT",
            TimeZone (-6 * 60) False "CST",
            TimeZone (-5 * 60) True "CDT",
            TimeZone (-7 * 60) False "MST",
            TimeZone (-6 * 60) True "MDT",
            TimeZone (-8 * 60) False "PST",
            TimeZone (-7 * 60) True "PDT"
            ]
        }
```

It's not hard to figure out how it works. We can also custom our own TimeLocale for Taiwan. :) 

```haskell
taiwanTimeLocale =  TimeLocale {
        wDays  = [("星期日", "周日"),
                  ("星期一", "周一"),
                  ("星期二", "周二"),
                  ("星期三", "周三"),
                  ("星期四", "周四"),
                  ("星期五", "周五"),
                  ("星期六", "周六")],
        months = [("一月", "一月"),
                  ("二月", "二月"),
                  ("三月", "三月"),
                  ("四月", "四月"),
                  ("五月", "五月"),
                  ("六月", "六月"),
                  ("七月", "七月"),
                  ("八月", "八月"),
                  ("九月", "九月"),
                  ("十月", "十月"),
                  ("十一月","十一月"),
                  ("十二月","十二月")],
        amPm = ("晝", "夜"),
        dateTimeFmt = "%a %b %e %H:%M:%S %Z %Y",
        dateFmt = "%m/%d/%y",
        timeFmt = "%H:%M:%S",
        time12Fmt = "%I:%M:%S %p",
        knownTimeZones =
            [
            TimeZone 0 False "UT",
            TimeZone 0 False "GMT",
            TimeZone (8 * 60) False "TWT" -- Taiwan Standard Time
            ]
        }
```
