---
title: Text Data in Haskell
author: Jaiyalas
date: 2016-04-12
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Haskell 提供了很多強大且方便的能力來讓我們可以抽象地處理資料。
但是，有時候這樣的便利性或多或少會犧牲掉效率，尤其是處理大量的文字資料時。
所以，究竟什麼時機要用什麼資料結構或工具來處理**文字資料**就變成了一個問題。

<!--more-->

Haskell 世界裡面有針對各種不同選擇而實作出來的各種不同文字結構。
這裡我們列出三個不同的考慮因素：

* 儲存格式 (Text or Binary) 
	+ 資料是以純粹 binary 的方式儲存而不帶任何其他資訊？ -- Binary
	+ 或是以人類語言(為了儲存人類語言，除了儲存資料本身以外，還需要儲存這些資料的表示方式例如格式等等資訊)方式儲存起來？ -- Text
* 封裝性 (Packed or Unpacked)
* 惰性 (Lazy or Strict)

--- 



(UNPACK)[https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/pragmas.html#unpack-pragma]

--- 

Char 在 GHC 底層是由 Int 轉換而來，而且支援表示 Unicode。換句話說，Char 本質上是個


---  

base 
	Data.Char(Data.String)
bytestring
	Data.ByteString
	Data.ByteString.Char8
	Data.ByteString.Lazy
	Data.ByteString.Lazy.Char8
text
	Data.Text
	Data.Text.Lazy
utf8-string
	Codec.Binary.UTF8.Generic
	Codec.Binary.UTF8.String
	Data.ByteString.Lazy.UTF8
	Data.ByteString.UTF8
	Data.String.UTF8


--- 

* [base (Prelude)](http://hackage.haskell.org/package/base)
* [bytestring](http://hackage.haskell.org/package/bytestring)
* [text](http://hackage.haskell.org/package/text)
* [utf8-string](http://hackage.haskell.org/package/utf8-string)
* [compact-string](http://hackage.haskell.org/package/compact-string) 

--- 

* [iconv](http://hackage.haskell.org/package/iconv)
* [encoding](http://hackage.haskell.org/package/encoding)



```haskell
Data.Functor.Contravariant
```

```haskell
class Contravariant f where
  contramap :: (a -> b) -> f b -> f a
  (>$) :: b -> f b -> f a
  (>$) = contramap . const
```

## 參考資料

* [How to pick your string library in Haskell by Edward Z. Yang](http://blog.ezyang.com/2010/08/strings-in-haskell/)
* 
