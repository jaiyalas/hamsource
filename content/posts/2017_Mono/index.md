---
title: Monomorphism in Haskell
author: Jaiyalas
date: 2017-04-10
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

這篇文章簡介了 haskell 做型別推導 (type inference) 的簡易過程，
以及這中間會產生的特點，與我們怎麼處理或使用這些特點。

<!--more-->

# $\forall$ 與 parametric polymorphic type

在 Haskell 中，任何一個 polymorphic typed function 例如

~~~{.haskell}
f x = (x,[x])
~~~

都會具有 type <type >f :: a -> (a, [a])</type>。
然後我們可以完全合法地寫出

~~~{.haskell}
exp = (f 1, f "str")
~~~

這裡關鍵是，pair 裡面左邊的 f 和右邊的 f 事實上具有不同的型別。
要理解這裡這個情況是怎麼回事，關鍵的一點是要先了解：
haskell 中每個沒被指明的 type variables 其實在最外面都有一個隱藏的 **$\forall$ quantifier**。
也就是說一個 type，以上面的 f 為例，事實上應該要寫成
<type >f :: forall a. a -> (a, [a])</type>。
因此， exp 可以看成是

~~~{.haskell}
exp = ( (f :: forall a. a -> (a, [a])) $ 1,
        (f :: forall a. a -> (a, [a])) $ "str")
~~~

而，因為這兩個 forall 是各自獨立的，所以 pair 的兩邊的 f 就會根據不同的參數而有了不同的型別：

~~~{.haskell}
exp = ( f 1     -- f :: Num t => t -> (t, [t]))
      , f "str" -- f :: String -> (String, [String]))
      )
~~~

這種由 "具有隱藏的 forall 的 type variable" 所構造出來的 polymorphic function 就被稱為 **parametric polymorphism**。

# type inference 與 generalization

haskell 推導 (infer) types 的簡易過程大概是：

1. 先根據等候右邊的 expression 去倒推出整個式子該有的 type signature
2. 檢查每個 type variables：
    + 如果不是 free 的： 就幫它加個 forall quantifier
    + 如果是 free 的，或是已經在 context 裡面出現了： 保持原狀不動

第 1 個步驟稱之為 (Hindley–Milner) **type inference** ；而第 2 個「幫 free type variables 加上 forall」的步驟被稱為 **generalization**。

## 舉例

上述這樣其實有點抽象，所以看個例子：

~~~{.haskell}
h x =
    let f y = (x, y)
    in f x
~~~

首先，haskell 看到第 1 行的時候會想說「阿，這邊有個 x ，那先假裝它的 type 叫做 t 好了」。
也就是說 haskell 會在 context 裡面加上 <type>x :: t</type> 這筆資訊。
而一進入到第 2 行， haskell 會可以馬上 infer 出 <type>f :: a -> (t, a)</type>。
接著，haskell 會開始試圖對 f 做 generalization，也就是，試著對所有 f 的 type variables 加上 forall：

**對 type variable <type>a</type> ：**    
顯然 a 不是 free 的，而是被 f (或說被 f 的那個隱藏 $\lambda$) 給綁住了。    
**對 type variable <type>t</type> ：**    
這裡的 t 是 free 的；而且我們知道這裡是 t 而不是任意一個 type b 是因為這個 type 是由 x 而來，而 x 的 type 已經在 context 裡面被定義好了，因此絕對不是"隨便一個 b"，而就會是"那一個 t" 。

因此，haskell 最終會 infer 出 <type >f :: forall a. a -> (t, a)</type> 以及 <type >h :: forall t. t -> (t, t)</type>。

# Monomorphism

承接上面的例子。對 f 來說， t 沒有被 forall 綁住會造成一些容易被我們的影響。例如說我們改寫一下 h 的定義：

~~~{.haskell}
h x =
    let f y = (x, y)
    in (f 1, f False)
~~~










<div class="block-notes">
<h3 class="block-notes"> Skolemization </h3>
Skolemization 是一種移除 $\exists$ (existential quantifiers) 的方法。

更明確一點地說，skolemization 是一個把 2nd order logic 中所有的 $\exists$移到 1st order $\forall$ 之外的過程。
給定一個 2nd order logic formula

$$\mathcal{A} \equiv \forall^1 \bar{x}\ .\ \exists^1 \bar{y}\ . \phi(\bar{x},\ \bar{y})$$

skolemization 會將其轉換成

$$ \exists^2 \bar{f}\ . \forall^1 \bar{x}\ . \phi(\bar{x},\ \bar{f}(\bar{y})) $$

或者，有時候可以將 $\bar{f}$ 當做是 global function 而可以寫成

$$ \forall^1 \bar{x}\ . \phi(\bar{x},\ \bar{f}(\bar{y})) $$

這兩個式子就被稱為 $\mathcal{A}$ 的 **Skolem normal form** (aka SNF)，或是直接叫做 **Skolem term**。
而其中 functions $\bar{f}$ 被稱為 **Skolem functions**；如果是 constant 而非 function 則被稱為 **Skolem constant**。

<h4 class="block-notes">Skolem type</h4>

...

</div>  <!-- class="block-notes" -->


<div class="block-notes">
<h3 class="block-notes"> **rigid type** 和 **wobbly type**  </h3>

簡單說就是：我們(身為 programmer)自己明確寫出來 type 就稱為 rigid type；反之，就都是 wobbly type。

兩者在 [Simon Peyton Jones 的 Simple unification-based type inference for GADTs ](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/gadt-pldi.pdf) 一文中都有明確解釋其定義：

+ Instead of “user-specified type”, we use the briefer term **rigid type** to describe a type that is completely specified, in some direct fashion, by a programmer-supplied type annotation.
+ A **wobbly type** is one that is not rigid. There is no such thing as a partly-rigid type; if a type is not rigid, it is wobbly

</div>  <!-- class="block-notes" -->











此時，整個 h 語法上還是合法的，也可以被正確編譯以及執行。但是，下面這段程式就不行了。

~~~{.haskell}
h x =
    let f y = [x, y]
    in (f 1, f False)
~~~

不行的原因其實相當簡單。前面是因為 x 和 y 是放在 pair 裡面，所以兩者之間的 types 會是各自獨立的，因此沒有問題。但是這裡的 x 和 y 是放在同一個 list 裡面，所以 x 和 y 的 type 必須要是同一個。也就是說，在 inference 的階段我們會得到 <type> f :: t -> [t]</type>。而這裡面出現的那個 t 會是 context 中已經有的，所以 t 不會被進一步 generalize (也就是說，對 f 而言 t 是沒有 forall 的)。那，t 的 forall 哪裡去了？事實上，它會是出現在 h 的 type signature 上面。這就表示，對整個 h 裡面來說，雖然我們可以任意選定一個 t 沒錯，但是在 h 的定義裡面的 t 都必須是同一個。這就好比說， cons 的 type 會是 <type> (:) :: forall a. a -> [a] -> [a] </type>，但是我們只能寫 `1 : [2]` 而不能寫 `'1' : [2]`。

... TBC ...
