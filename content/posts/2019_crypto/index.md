---
title: "Hash with Tezos-client"
date: 2019-10-03
description: ""
categories: ["Crypto"]
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Assuming that we want to compute a sha256 hash of string, say "this is for tezos",. Natually we will run something like

```bash
> echo -n 'this is for tezos' | openssl dgst -sha256
(stdin)= 30e02e414701f70a1d784e5f92405c01dd89f4c8bc17528bbf6720d4782295a1
```

> 30e02e414701f70a1d784e5f92405c01dd89f4c8bc17528bbf6720d4782295a1

But now, let's compare what we will have from asking tezos-client to do this for us:

```bash
> tezos-client005 -A node2.sg.tezos.org.sg hash data '"this is for tezos"' of type string
Raw packed data: 0x0501000000117468697320697320666f722074657a6f73
Script-expression-ID-Hash: exprtcfVrbCucKrXo7pKx8QWrr7qzwKDSipnu6ryMzGUYCz6WLsDKF
Raw Script-expression-ID-Hash: 0x0d2364977a1aa77034e30d5152d1e89c524afd839d2045e3888f017509da6626
Ledger Blake2b hash: tHbuLEB3dCfC7U4iiDXfqC3TaEYdntx68TJVShe3FnV
Raw Sha256 hash: 0xe8c6d7e2fc367eb24d20de646dd2510ea0c83ac09ed919aacf29f1a1eac792e4
Raw Sha512 hash: 0x6bc51d5f5dce4a990eda272b02413529569299eea94c68b75da4ef7d034660a02331c83cf0c97da49bdfeff08e34280ece24b687b3a180fbc6d90df5a5e2a920
Gas remaining: 799906 units remaining
```

> e8c6d7e2fc367eb24d20de646dd2510ea0c83ac09ed919aacf29f1a1eac792e4

Well, obviously, the result doesn't match! Even if we run the following instead, they still don't match.

```bash
> echo -n '"this is for tezos"' | openssl dgst -sha256
(stdin)= 3fb5a3a9ce9e920528d93c52eafec2255fab6d3428cdc4c37d2396f73f81516a
```

> 3fb5a3a9ce9e920528d93c52eafec2255fab6d3428cdc4c37d2396f73f81516a

## tezos-client

so, how tezos-client did it? Let's find out!

Firstly, let's apply `-l` argument to tezos-client then we can know that the last thing tezos-client does for hash data is calling RPC for getting packed data back.

```bash
>>>>3: http://node2.sg.tezos.org.sg:8732/chains/main/blocks/head/helpers/scripts/pack_data
  { "data": { "string": "this is for tezos" }, "type": { "prim": "string" },
    "gas": "800000" }
<<<<3: 200 OK
  { "packed": "0501000000117468697320697320666f722074657a6f73",
    "gas": "799906" }
```

So, it's quite clear that all the hash functions are computed locally by tezos-client. Now for understanding what tezos-client will do, let's take a look at this file: [client_proto_programs_commands.ml](https://gitlab.com/tezos/tezos/blob/babylonnet/src/proto_005_PsBabyM1/lib_client_commands/client_proto_programs_commands.ml#L315).

Well, it turns out the tezos-client is using bytes, `0501000000117468697320697320666f722074657a6f73`, for hashing rather than string, `'this is for tezos'`.



```bash
> echo -n '0501000000117468697320697320666f722074657a6f73' | xxd -r -p
this is for tezos
```

```bash
> echo -n '0501000000117468697320697320666f722074657a6f73' | xxd -r -p | openssl dgst -sha256
(stdin)= e8c6d7e2fc367eb24d20de646dd2510ea0c83ac09ed919aacf29f1a1eac792e4
```

```bash
echo -n '7468697320697320666f722074657a6f73' | xxd -r -p
this is for tezos
```

```bash
echo -n '7468697320697320666f722074657a6f73' | xxd -r -p | openssl dgst -sha256
(stdin)= 30e02e414701f70a1d784e5f92405c01dd89f4c8bc17528bbf6720d4782295a1
```

```bash
echo -n "this is for tezos" | od -A n -t x1
    74  68  69  73  20  69  73  20  66  6f  72  20  74  65  7a  6f
    73
```

> 7468697320697320666f722074657a6f73
