---
title: Quick notes on Pandoc
author: Jaiyalas
date: 2017-04-04
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Pandoc, 相當好用的 haskell 程式。可以讓使用者在 markdown(+lhs), tex, pdf, epub 這些格式之間相互轉換。
但是 pandoc 使用上有很多參數和小細節需要注意，這篇文章簡單整理一些重點以供未來參考與翻閱。

<!--more-->

# 懶人直接抄

## `markdown` to slide

~~~{.bash}
> pandoc --mathml -i -t beamer -V theme:Warsaw -V colortheme:beetle -s -o slide.pdf slide.md
~~~

或

~~~{.bash}
> pandoc --mathml -i -t revealjs -V theme=sky --highlight-style espresso -s -o slide.html slide.md
~~~

## `markdown+lhs` to `pdf`

懶人法

~~~{.bash}
> pandoc -f markdown+lhs -s --highlight-style kate -o OutFile.pdf InFile.lhs
~~~

要 **使用 tikz** 或是要 **使用中文** 或是要 **自定 latex 設定**

~~~{.bash}
> vim tikz.preamble
~~~

加入

~~~{.tex}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{matrix}
\usetikzlibrary{arrows}
~~~

~~~{.bash}
> pandoc -f markdown+lhs --latex-engine=xelatex -B tikz.preamble -V CJKmainfont='Hiragino Maru Gothic ProN' -V papersize=A4 -V geometry:margin=2cm --highlight-style espresso -s -o Output.pdf Input.lhs
~~~

## `markdown+lhs` to `html`, `epub` or `docx` (word)

~~~{.bash}
> pandoc -f markdown+lhs --highlight-style kate InFile.lhs -s -o OutFile.html  
> pandoc -f markdown --highlight-style kate InFile.md -s -o OutFile.epub
> pandoc -f markdown --highlight-style kate InFile.md -s -o OutFile.docx
~~~

# 基本參數

## 輸入相關參數

+ `-f FORMAT` 指定輸入格式
    + 使用 `pandoc --list-input-formats` 可以瀏覽所有輸入格式
    + 使用 `pandoc --list-extensions` 可以瀏覽所有擴充與其設定
+ `+lhs` 指定輸入格式為 lhs 檔
    + 只能用於 `markdown`, `rst`, `latex`, 或 `html`
    + 開啟時會強制取消 markdown 的 ATX-style (header 不能用 `# ooxx` 那種)

## 輸出相關參數

+ `-s` 產生獨立 (standalone) 檔案而非程式碼片段 (fragment)
    + 輸出格式為 html, latex, rtf 有效
+ `-o` 輸出到指定檔案
    + 不指定時會直接輸出到 stdout
+ `-t FORMAT` 指定輸出格式
    + 不指定時直接根據輸出檔案的副檔名決定
    + 使用 `pandoc --list-output-formats` 可以瀏覽所有輸出格式

## 內容相關參數

+ `--toc` 開啟 toc
    + `--toc-depth=NUMBER` 指定有效 toc 深度
    + `-N` 在標題前加上計數數字
+ `--highlight-style STYLE` 指定 syntax-highlight style
    + `--no-highlight` 強制關閉 syntax-highlight
+ `--atx-headers` markdown using ATX-Headers
    + 啟用 `+lhs` 時會自動關閉 ATX-style 的支援
+ `--wrap=auto|none|preserve` 設定 wrap 方式
    + `--no-wrap` 關閉 wrap 功能

### 變數設定

+ `-V KEY=[VALUE]` 增加/指定 template variable
    + latex 可用變數： [http://pandoc.org/MANUAL.html#variables-for-latex](http://pandoc.org/MANUAL.html#variables-for-latex)
    + 投影片可用變數： [http://pandoc.org/MANUAL.html#variables-for-slides](http://pandoc.org/MANUAL.html#variables-for-slides)
    + pandoc 可用變數： [http://pandoc.org/MANUAL.html#variables-set-by-pandoc](http://pandoc.org/MANUAL.html#variables-set-by-pandoc)
    + template 可用變數： [http://pandoc.org/MANUAL.html#using-variables-in-templates](http://pandoc.org/MANUAL.html#using-variables-in-templates)
+ `-M KEY[=VALUE]` 增加/指定 metadata field
    + template 中可以讀到
    + filter 中可以讀到
    + writer 中不見得可以讀到

### 引入額外文件內容 (latex/html)

+ `-B FILE` 貼上檔案內容至文件主體前
    + (latex) `\begin{document}` 前
    + (html) 緊貼在 `<body>` 後面
+ `-A FILE` 貼上檔案內容至文件主體後
    + (latex) `\end{document}` 後
    + (html) 緊貼在 `</body>` 前面

# 特殊格式專用參數

## 指定輸出樣板 (template)

+ `--template=FILE` 指定樣板檔案
    + 預設樣板檔案可以在 [https://github.com/jgm/pandoc-templates](https://github.com/jgm/pandoc-templates) 找到
    + 整理好的額外好用樣板 [https://github.com/jgm/pandoc/wiki/User-contributed-templates](https://github.com/jgm/pandoc/wiki/User-contributed-templates)
    + 樣板中可以使用的變數介紹 [http://pandoc.org/MANUAL.html#templates](http://pandoc.org/MANUAL.html#templates)

### (範例) 藉由自定樣板來支援 tikz

首先，輸出預設 latex 用 template 檔案：

~~~{.bash}
> pandoc -D latex > tikz.latex
~~~

接著，打開 `tikz.latex` 並且在 preamble 區域加入以下程式碼：

~~~{.tex}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{matrix}
\usetikzlibrary{arrows}
~~~

最後就只要在使用 pandoc 產生 pdf 時多加上參數 `--template=tikz.latex` 即可。

## epub 專用輸出參數

+ `--epub-cover-image=FILE` 指定封面圖
+ `--epub-stylesheet=FILE` 指定額外的 css 檔案
    + `--epub-embed-font=FONTFILE` 包入指定的字型檔案 (需要在 css 中設定使用)
+ `--epub-metadata=FILE` 指定額外的 metadata 檔案

## latex (pdf) 專用輸出參數

+ `--latex-engine=pdflatex|lualatex|xelatex` 指定 latex 引擎
+ 要使用中文需要開啟最少：
    + `--latex-engine=xelatex`
    + `-V CJKmainfont='Lihei Pro'`

## slide 專用輸出參數(與編輯技巧)

+ `-i` 漸進式 bullet 呈現
+ 將 `. . .` 作為獨立段落 = 暫停
+ reveal.js 或 beamer 的設定參數可以用 `-V` 設定
    + e.g. `-V theme=sky` 或 `-V theme:Warsaw`
    + reveal.js congif 可以參考 [https://github.com/hakimel/reveal.js#configuration](https://github.com/hakimel/reveal.js#configuration)
    + beamer theme 可以參考 [http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html](http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html)

## 使用 filter

+ `--filter=PROGRAM` 指定外部 filter 程式
    + 會在 parse 好但是還沒交付給 writer 之前使用。
    + 已經由好人寫好的第三方 filter 可以在 [https://github.com/jgm/pandoc/wiki/Pandoc-Filters](https://github.com/jgm/pandoc/wiki/Pandoc-Filters) 找到

# 常見格式列表

## 輸入格式

+ native (native Haskell)
+ json
+ markdown
+ markdown_strict
+ markdown_github
+ html
+ docx
+ odt
+ epub
+ haddock
+ latex

## 輸出格式

+ native (native Haskell)
+ json (JSON version of native AST)
+ plain (plain text)
+ markdown (pandoc’s extended Markdown)
+ markdown_strict (original unextended Markdown)
+ markdown_github (GitHub-Flavored Markdown)
+ rst (reStructuredText)
+ html (XHTML)
+ html5 (HTML5)
+ latex (LaTeX)
+ opendocument (OpenDocument)
+ odt (OpenOffice text document)
+ docx (Word docx)
+ haddock (Haddock markup)
+ rtf (rich text format)
+ epub (EPUB v2 book)
+ epub3 (EPUB v3)

### slide 專用格式 (html 或 pdf)

+ beamer (LaTeX beamer slide show)
+ slidy (Slidy HTML + JavaScript slide show)
+ slideous (Slideous HTML + JavaScript slide show)
+ dzslides (DZSlides HTML5 + JavaScript slide show)
+ revealjs (reveal.js HTML5 + JavaScript slide show)
+ s5 (S5 HTML + JavaScript slide show)

## highlight-style

+ pygments
+ tango
+ espresso
+ zenburn
+ kate
+ monochrome
+ breezedark
+ haddock
