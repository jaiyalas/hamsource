---
title: "Reveal Your Hackage Dependency"
date: 2019-07-12
description: "The graphmodis a tool to generate a report for a hackage."
categories: ["Haskell"]
displayInMenu: false
displayInList: true
draft: false
---

The graphmod is a tool to generate a report for a hackage. You can find it on [hackage](http://hackage.haskell.org/package/graphmod), [github](https://github.com/yav/graphmod) and [wiki](https://github.com/yav/graphmod/wiki).

To install via Stack

```bash
$ stack build --copy-compiler-tool graphmod
```

To generate default report:

```bash
$ stack exec graphmod | tred | dot -Tpdf > modules.pdf
```

To generate report without module:

```bash
$ stack exec graphmod -- --no-cluster | tred | dot -Tpdf > modules.pdf
```
