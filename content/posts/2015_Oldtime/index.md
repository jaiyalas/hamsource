---
title: For old time's sake.
author: Jaiyalas
date: 2015-05-29
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Today we will have a short overview of old data-/time-related packages listed as follows:

* [old-time](https://hackage.haskell.org/package/old-time) 
* [time-compat](http://hackage.haskell.org/package/time-compat)
* [old-locale](https://hackage.haskell.org/package/old-locale) 
* [time-locale-compat](http://hackage.haskell.org/package/time-locale-compat) [^1]

Those modules are very easily to use but have some shortcomings. The **old-time**(`System.Time`) has poor supporting on locale and formating and lack functions to manipulate times. And the `TimeDiff` is not very sueful. The **old-locale**(`System.Locale`), however, is not very out-dated.  

[^1]: It's an simple example for `{-# LANGUAGE CPP #-}`.

<!--more-->

### old-locale

The **old-locale** package provides module `System.Locale` allowing to adapt to local conventions. There is just one single structure defined.

#### The `TimeLocale`

```haskell
data TimeLocale = TimeLocale {
        wDays  :: [(String, String)],
        months :: [(String, String)],
        intervals :: [(String, String)],
        amPm   :: (String, String),
        dateTimeFmt, dateFmt,
        timeFmt, time12Fmt :: String
        } ...
``` 

The difference bewteen `Data.Time.Format.TimeLocale` and `System.Locale.TimeLocale` is very ignorable. The most significant one is that the has `knownTimeZones :: [TimeZone]` instead of `intervals :: [(String, String)]`.

#### Extra

The package [time-locale-compat](http://hackage.haskell.org/package/time-locale-compat) is a interface for importing different versions of `TimeLocale`. If the version of **time** is greater then **1.5.0**, it will import `Data.Time.Format` and import `System.Locale` for otherwise. 

### old-time

The **old-time** package provide the `System.Time` module that contains three distinct structures for representing times:

#### The `ClockTime`

The first one is used to represent raw system time in two integers.

```haskell
data ClockTime = TOD Integer Integer
``` 

One can extract system clock time by using 

```haskell
getClockTime :: IO ClockTime
```

Besides `ClockTime` there is also a structure for recording differenct between two clock times

```haskell
data TimeDiff
 = TimeDiff	{
       tdYear    :: Int
     , tdMonth   :: Int
     , tdDay     :: Int 
     , ...
 }
```

#### The `CalendarTime`

The second structure in this module is `CalendarTime` which will store time in a human-friendly structure:

```haskell
data CalendarTime
 = CalendarTime  {
       ctYear    :: Int
     , ctMonth   :: Month
     , ctDay     :: Int 
     , ...
 }
```

Also, this module defines several converting function between `CalendarTime`, `ClockTime` and `String`. Read [old-time](https://hackage.haskell.org/package/old-time) for more details.

#### Extra

There is a tiny package, [time-compat](http://hackage.haskell.org/package/time-compat), providing single typeclass `ToUTCTime` that define a function `toUTCTime` to convert an old-time structure to a `Data.Time.UTCTime`. 
  
