---
title: The darkness under the blanket
author: Jaiyalas
date: 2016-04-12
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Haskell 初學的時候一定是一路越學越抽象。但是很可惜的是，當學習和實作到了某個時間點時，會突然發現碰到很多神奇的髒髒小東西。這些小東西會影響效能，甚至是計算的結果和資料呈現的方式。這一篇筆記試圖解釋這些有點討厭又常常看到的名詞例如 thunk, boxing, packing, lifting 等等...

<!--more-->

在進入正題之前，我們需要先知道一些基本的知識。
首先，以 GHC 來說，Haskell code 都會被以 STG 語意來執行。
所以這篇文章只會深度到 STG machine 的層次。(<del>再深我也不懂 XD</del>)

在 GHC (或說 STG machine) 中，所有儲存在 run-time memory (也就是 heap) 中的東西都被視為是一個 **closure** 而且被稱為一個 **heap object**。一個 heap object 由兩個部分構成：

* **header** -- 一個*指向關於這個 closure 的 info table* 的 pointer (稱為 info pointer)。
* **payload** -- 這個 closure 的 environment，包含 free variables (pointers) 和 arguments (values)。

這裡需要額外說明的是 **Info(rmation) table**。 Info table 中儲存了關於該 closure 的其他所有資訊，而其中有兩項格外重要。其一是 **closure type**，另外還有就是 **entry code**。顧名思義，closure type 描述的是這個 closure 的類別 -- 可以是 thunk, function,  constructor 等等。而 entry code 則微妙一點。首先， entry code 指的是一段 STG machine code，而且這段 code 會在程式到達某個特定 "entry" 時被執行。這裡的重點是，對於不同種類的 closure 來說，那個 "entry" 會是指不同的情況。例如說：

* thunks -- 當這個 thunk 被要求要計算出值時。
* functions -- function 的 info table 中會記錄這個 function 的參數數量(arity)，而 一個 function 的 "entry" 就是：當這個 function 被給定全部參數且被要求計算(applied)時


## Lazyness, ⊥ and Datatypes

資料結構。
理想上我們只需要 algebraic datatypes。
但是現實上，不管是效能因素或是實作因素，我們就是會需要 primitive datatype。








Lifting 是源自 domain theory 的一個 construction (operator)。
Lifting 拿一個 CPO 然後在其上面多加一個 ⊥ 上去。

從 data structure/type 的觀點來說，
lifting 相當於是拿一個任意的 type 然後把它包裝成一個 pointed type



lifting : primitive datatype -> algebraic datatypes


lifted datatype 一定是 pointed
但是 pointed datatype 不一定是 lifted
例：兩個 pointed datatype 的 pair 也會是 pointed 但是不是 lifted




haskell 有兩個 "kinds"：`*` 和 `#`

* `*` 
	+ lifted 
	+ boxed values
* `#`
	+ unlifted
	+ unboxed values





故事要從 ⊥ 說起。
身為一個 lazy functional language，haskell 每個 datatype 都具有一個 ⊥。
這樣的設計讓 haskell 可以自然的提供 algebraic datatype (datatypes that can be defined by type functors) 也很直觀的讓 denotational demantics (domain theory)可以對應過來。

不過這樣的設計當然是需要付出代價的。

`Int` 是 lifted type 
`Int#` 是 unlisted type





## Pointed and Unpointed

pointed 這個詞是從 pointed CPO 來的。
所謂 pointed CPO 就是指「有 ⊥ 的 CPO」。
在 domain theory 中，CPO 都是指 pointed CPO。

所以，延伸來看，所謂 pointed (data)type，
很自然就是指「有 ⊥ 的 (data)type」。
而 unpointed (data)type 則是指沒有的。

我們概念上可以把這些東西如是連結起來：

* Pointed (data)type 就是指可以用 domain theory 來描述其 denotional semantics 的 (data)type。換句話說，基本上就是那些 algebraic datatype (也就是 type functors)。
* Unpointed (data)type 則是指


Pointed/Unpointed 是抽象的概念。
我們說一個 variable "是 pointed" (of pointed type)，最直接的意思是說這個 type 有 



algebraic datatype (datatypes that can be defined by type functors)




## Boxed and Unboxed 

a boxed type is represented by a pointer to a heap object. 
an unboxed object is represented by a value. 
For example, Int is boxed, but Int# is unboxed. 

Unboxed types correspond to the “raw machine” types you would use in C: Int# (long int), Double# (double)

The main restriction is that you can't pass a primitive value to a polymorphic function or store one in a polymorphic data type. This rules out things like [Int#]. The reason for this restriction is that polymorphic arguments and constructor fields are assumed to be pointers: if an unboxed integer is stored in one of these, the garbage collector would attempt to follow it, leading to unpredictable space leaks.



*value* 在 haskell 中分成 boxed 和 unboxed 兩種。

boxed value = a pointer (to a heap obj)
unboxed value = raw bit data

all pointed values are boxed

boxed value 才有 info pointer 



### GHC 壞壞

GHC 裡面有幾個 moduel 描述了這些 low-level types 相關的東西。
而這會讓人家有一點混淆。


## Thunks

關於 Thunk 有幾種不同的說法：

* a thunk is a value that is yet to be evaluated. -- [haskell wiki](https://wiki.haskell.org/Thunk)
* a thunk is a placeholder object, specifying not the data itself, but rather how to compute that data. -- [haskell wikibook](https://en.wikibooks.org/wiki/Haskell/Strictness)
* a thunk is a subroutine that is created, often automatically, to assist a call to another subroutine. -- [wikipedia](https://en.wikipedia.org/wiki/Thunk)

顯然，我們可以概念性地知道 thunk 是一個用來表示(或說儲存)*還沒算好的值*的東西(甚至有種說法是直接把 thunk 當做是 expression 的同義詞)。這點可以利用 ghci 中的 `:print -- show a value without forcing its computation
` 這個指令來讓我們一窺究竟：

```haskell  
Prelude> let x = (1+2, 3) :: (Int,Int)
Prelude> :print x  
x = ((_t12::Int),3)
```

這個 `_t12::Int` 就是一個 thunk。這個 thunk 儲存了一個「值」，但是因為這個值還沒有要用所以 haskell 還沒有把她算出來。而這個值一旦被要求要算出來，那麼這個 thunk 就會被計算出來然後被取代(updating)而消失(被 gc 掉)。

```haskell
Prelude> x
(3,3)
Prelude> :print x
x = (3,3)
```

總結來說，Thunks 是 haskell run-time 時所產生出來協助計算與管理的物件，並且會被以 closure (heap object) 的形式儲存在 heap 中。Thunks 是 haskell 中實踐 lazyness 的關鍵概念，他讓我們可以在 run-time 描述並且操作一個還沒被計算完的式子。當然這樣也是會有缺點，尤其是在計算效率上面，更多細節與範例可以參考 [Performance/Strictness](https://wiki.haskell.org/Performance/Strictness) 這篇文章。

## 參考資料

* [I know kung fu: learning STG by example](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/GeneratedCode)
* [The Layout of (GHC) Heap Objects](https://ghc.haskell.org/trac/ghc/wiki/Commentary/Rts/Storage/HeapObjects)
* [GHC illustrated](https://takenobu-hs.github.io/downloads/haskell_ghc_illustrated.pdf)
