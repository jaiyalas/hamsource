---
title: How to sample an element from varied structures.
author: Jaiyalas
date: 2015-05-27
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Assume that function `sample` can pick a element from a massive structure. To define such `sample` for different structures, one would naturally try to define a typeclass: 

```haskell
class Samplible a where
  sample :: a -> b
```

<!--more-->

This typeclass, however, is too generalized to use. The following instance is illegal since ghc cannot match `head :: forall a.[a] -> a` with `sample :: forall a b. a -> b`.
```haskell
instance Samplible [a] where
  sample = head -- ERROR
```
Therefore, we can define another typeclass as
```haskell
class Samplible a b where
  sample :: a -> b
```
This one require programmer to explicitly give `a` and `b`. But they don't have to have any relation. So, how nice it would be if we can put some contraints on `b`?

## Functional Dependency 

The very first attempt is that we can just add some *functional dependency* into that typeclass. Functional dependencies can define relationship between type variables.
```haskell
class Samplible a b | a -> b where
  sample :: a -> b
```
In this case, we declare that type variable `b` must be somehow related to `a`. In other words, `b` only can be a concrete type, such as `Int` or `Bool`, or any type variables occurred in `a`. For example,
```haskell
instance Samplible [a] a where -- VALID
  sample = head
instance Samplible (a,b) a where -- VALID
  sample = ...
instance Samplible (a,b) b where -- VALID
  sample = ...
instance Samplible (a,b) c where -- ERROE!!
  sample = ...
instance Samplible (a,Int) Char where -- VALID
  sample = ...
```

## Associated type synonym

Haskell also provide another mechanism, *type synonym*, to manipulate type variables within typeclass. The main idea is to use that type synonym to "capture" (or "present") the type of element.  
```haskell
class Samplible a where
  type Elem a :: *
  sample :: a -> Ele a
instance Samplible [a] where
  type Elem [a] = a
  sample = head
instance Samplible ByteString where
  type Elem ByteString = Word8
  sample = B.head . B.unpack
instance Samplible Integer where
  type Elem Integer = Word8
  sample = fromInteger  
```
A type declared inside a typeclass is called an *associated type*. In fact, that type is a *type family*, but this is beyond the scope of this post. If you want to understand more detail about type family, please read [this post](.). 

## Comparing with Associated type synonym and Funtional dependency

In fact, both of these ways has same expressivity. However associated type synonym would be a better choice because of functional dependencies may cause human-understanding problems if we get too much dependencies. 
 
## Equality constraints 

Using associated type synonym is great, but it also have a shortcut. Comparing with function dependency one can easily find out that we lost a typeclass parameter, `b`. To expose that parameter, we introduce some equality contraints as follows 
```haskell
class (Elem a ~ b) => Samplible a b where
  type Elem a :: *
  sample :: a -> b
```
