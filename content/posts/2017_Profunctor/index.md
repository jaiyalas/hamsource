---
title: Profunctors and its relatives
author: Jaiyalas
date: 2017-06-16
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

Functor 是 haskell 中很常見的基本型別操作，而實務上 functors 有幾個好用的特例。
這篇文章會介紹 (covariant) functors, contravariant functors, bifunctors 和 profunctors 這四種之間的定義與(個人整理之)使用的時間。 

<!--more-->

# Functors

所謂 functors 是兩個資料結構之間的轉換。一個完整的 functor 包含**結構的轉換**和**計算的轉換**。

一種理解 functors 的方式是：定義新的資料結構。

## Covariant functor

我們把一個 functor F 被當做 type-level function，
有兩個部分

data F a = C0 vec0 ...

instance Functor F where
	fmap f = ...

+ algebraic datastructure 一定是 covariant functor



## Contravariant functor

+ functor made of arrows 且 type variable are on negative position 一定是 contravariant functor


# Bifunctor

# Profunctor
