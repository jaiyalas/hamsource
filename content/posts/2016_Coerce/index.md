---
title: Newtype and coerce
author: Jaiyalas
date: 2016-08-27
categories: ["Haskell"]
toc: false
dropCap: false
displayInMenu: false
displayInList: true
draft: false
---

This post is written for explaining how to manipulate datatype with newtype and coerce.

<!--more-->

# Prelude (pattern matching)

## `case` vs `let`

說到 pattern matching，大家都知道是 haskell 核心的功能之一。不管是一般 function definition 時寫在等號左手邊或是 lambda function 裡面的 pattern matching，基本上都和 `case .. of ..` 是同樣的 ***strict*** pattern matching。換句話說，給定

+ case **v** of **p** → **e**
+ (\\ **p** . **e**) **v**

不管 **e** 之中有沒有用到 **p** 裡面的東西，**v** 一定要先被 match 到 pattern **p** 上面，然後才會執行 **e**。

另一方面，haskell 中的 `let .. in ..` 則是 ***lazy*** pattern matching。所有 `let` 都會被轉換成 `case`，同時會把 pattern 轉換成 lazy pattern ：
<div class="block-definition">let **p** = **v** in **e** = case **v** of ~**p** -> **e**。</div>

## Irrefutable patterns

在 haskell 中有一部分的 pattern 被稱為是 irrefutable (無法駁倒的)。例如說：

+ variable
+ wild-pattern `_`
+ lazy-pattern `~p` (for any pattern `p`)

是三種基本的 irrefutable pattern。Irrefutable matching 有些有趣的地方，首先，它其實暗指了這個 pattern *一定要被 match 成功*。
而如果一個 irrefutable matching 失敗了，會丟出特定的錯誤訊息，而不是一般的 matching 錯誤。
例如說，我們會可以寫出這種奇怪的情況：

```{.haskell}
Prelude> case (False, 1) of {(True,p) -> p}
*** Exception: Non-exhaustive patterns in case

Prelude> case (False, 1) of {~(True,p) -> p; (False,p) -> p}
*** Exception: Irrefutable pattern failed for pattern (True, p)
```

除了上面的基本款之外，我們還有兩種 inductively 定義出 irrefutable pattern 的方法，as-pattern 和 newtype：

### The as-pattern

一個 as-pattern `x@ip` 是 irrefutable 如果 `ip` 是 irrefutable。
這其實不難理解，因為 **x@** 的部份其實就只是多 bind 一個變數而已。
我們可以從 Haskell 2010 language report 裡面的 [Formal Semantics of Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-610003.17.3) 中的 rule:

<div class="block-definition">(e): case **v** of { **x@ p** -> e; _ -> e′ } = case **v** of { **p** -> (\\ **x** -> e) **v**; _ -> e′}</div>

來理解 haskll 事實上做了什麼樣的轉換。不過為了簡單獲得操作性的理解，我們可以直接用個連續的例子來試著看看會發生什麼事情！

首先，一般的情況下，我們的 strictly 和 lazily matching 會像這樣下面這樣。這邊請順便留意一下兩個 exceptions 是不一樣的！

```{.haskell}
Prelude> case [1] of (1:ys) -> 1:ys
[1]
Prelude> case [2] of (1:ys) -> 1:ys
*** Exception: Non-exhaustive patterns in case
Prelude> case [1] of ~(1:ys) -> 1:ys
[1]
Prelude> case [2] of ~(1:ys) -> 1:ys
[1*** Exception: Irrefutable pattern failed for pattern (1 : ys)
```

接著，我們試著在 strictly matching 時用 as-pattern。然後我們可以發現說，如果 pattern `(1:ys)` 有可以被 matching 成功就是成功，不然就 matching 失敗。也就是說，如果 pattern 本身還是 refutable patterns，那不管我們用了 `x` 還是 `ys` 結果都一樣。

```{.haskell}
Prelude> case [1] of x@(1:ys) -> 1:ys
[1]
Prelude> case [1] of x@(1:ys) -> x
[1]
Prelude> case [2] of x@(1:ys) -> 1:ys
*** Exception: Non-exhaustive patterns in case
Prelude> case [2] of x@(1:ys) -> x
*** Exception: Non-exhaustive patterns in case
```

最後，我們用 lazy-pattern 把 refutable patterns `(1:ys)` 給變成 irrefutable patterns。
這時候我們就會發現 expression 裡面用的是 `x` 還是 `ys` 是有差別的！
就算 `(1:ys)` 會 matching 失敗，因為我們其實沒有真的去 match 它，所以可以整個裝傻當做是 matching 成功。

```{.haskell}
Prelude> case [2] of x@ ~(1:ys) -> 1:ys
[1*** Exception: Irrefutable pattern failed for pattern (1 : ys)
Prelude> case [2] of x@ ~(1:ys) -> x
[2]
```

### The newtype

一個由 newtype constructor 作為 head 的 pattern，例如說 `N p` (`N` 是 newtype 的 constructor)，是 irrefutable，
如果其 inner pattern `p` 已知是 irrefutable pattern。 Newtype 的 constructor 是個有趣的東西，更多細節請看下一節，在這裡我們只要知道說：假設我們有 `newtype N = N t`，那麼，出現在某 pattern，裡面的 `N` 會將某個 `v : N` 變成是 `v : t`。

確實，我們理論上希望 newtype constructor 只是一個單純的 label，所以如果 `p` 是 irrefutable 那麼 `N p` 也應該要是 irrefutable，反之亦然。再一次，我們還是可以從 Haskell 2010 language report 裡面的 [Formal Semantics of Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-610003.17.3) 中的 rule:

<div class="block-definition">(k): case **N** *v* of { **N** *p -> e*; _ -> e′ } = case *v* of { *p -> e*; _ -> e′ }<br/>(l): case **⊥** of { **N** *p -> e*; _ -> e′ } = case **⊥** of { *p -> e* }</div>

去了解 haskell 實際上是怎麼辦到這件事情的。

---

總結來說，與其說是 at-pattern 和 newtype 會可以用來產生 irrefutability，還不如說：at-pattern 和 newtype 會保留 pattern 的 (ir)refutability。因為 at-pattern 和 newtype 其實沒有對於 pattern 增加或減少任何資訊，所以 haskell 在 pattern matching 的時候就簡單的把 `x@` 和 `N` 給直接拿掉或是換成沒影響的東西。

# The `newtype`

`newtype` 是 haskell 中用來複製 algebraic datatype 的機制。
而這個被複製出來的 algebraic datatype 習慣上我們稱之為 wrapper。

<div class="block-notes">
一個很經典的例子就是 Prelude 裡面的 `Bool` 和 Data.Monoid 裡面的 `All` 與 `Any`。
</div>

概念上，wrapper 就是在某個原有的資料結構的最外面包上一個新的 constructor，
而這件事情其實在 haskell 中也可以簡單的用 data 辦到。
所以，到底為什麼我們要創造出一個複製資料結構專用的弱化版的 data 呢？
這件事情要從用 data 做 wrapper 會產生出來的缺點說起。

## wrap by `data`

`data` 所產生的 constructor 是一個 [lifting operator](http://jaiyalas.github.io/content/posts/2016-04-13-liftpoint.html#pointed-domain-and-lifting)，







在 haskell 中 data wrapper 可以用 `data` 和 `newtype` 來實現。
不管是哪個方法，都有清空原本 instances 的效果。

例如說，假設我們有一個頗複雜的資料結構 `Original` 如下

```{.haskell}
data Original = MkOgl1 ...
              | MkOgl2 ...
              | MkOgl3 ...
              ... deriving (Cla, Clb, Clc)
```

我們分別可以定出兩個 wrappers:

```{.haskell}
newtype NtOgl = MkNtOgl Original
```

```{.haskell}
data DtOgl = MkDtOgl Original
```


## as *wrapper* or as *datatype renaming*

As a wrapper, one can wrap it in a newtype  and it'll be considered distinct to the type-checker, but identical at runtime.

```{.haskell}
newtype T = N A
```

+ In expression, `N` coerces a value from type `A` to type `T`.
    + 是一個 type wrapper：將 datatype `A` 用 constructor `N` 給重新包裝成 `T`。 而 A 和 T 在 run-time 是一樣的東西(have same representation)
+ Within pattern, `N` coerces a value from type `T` to type `A`.
+ 看情況，有時候這些 coercions 會是免費的(no run-time overheads)

### Difference between `newtype` and `data`

Constructing a `data` is a *lifting* operation; `newtype`, however, is *unlifting*.

Which means, given a datatype `A` with its constructor `CA`,
the constructor `D` in `data D = D A` will translate(?) `⊥ : A` into `D ⊥ : D`.
This `D ⊥ : D` will differ from `⊥ : D`.


可以舉例子 as domain ： Bool 和 Maybe Bool 中的 bot

+ clone datatype itself without run-time cost
+ as a way to define ad-hoc poly. (operator overloading)

## why wrapping? renaming?

我們可以將它視為是一種 reset typeclasses

```{.haskell}
data Bool : *
```

在 `Data.Monoid` 之下，有兩個 Bool wrappers

```{.haskell}
class Monoid a where
        mempty  :: a
        -- ^ Identity of 'mappend'
        mappend :: a -> a -> a
        -- ^ An associative operation
```

```{.haskell}
newtype All = All {getAll :: Bool}
newtype Any = Any {getAny :: Bool}

-- | Boolean monoid under conjunction ('&&').
newtype All = All { getAll :: Bool }
        deriving (Eq, Ord, Read, Show, Bounded, Generic)

instance Monoid All where
        mempty = All True
        All x `mappend` All y = All (x && y)

-- | Boolean monoid under disjunction ('||').
newtype Any = Any { getAny :: Bool }
        deriving (Eq, Ord, Read, Show, Bounded, Generic)

instance Monoid Any where
        mempty = Any False
        Any x `mappend` Any y = Any (x || y)

```

## things in newtype which are not that good

缺點

+ clean up all provided instances
+ some standard typeclass can be derivable, but most not
+ for those instances we don't want to modify computation underneath,
    + we still have to write instances for them.
    + This will create overhead; and,
    + we will get no promise on that GHC is smart enough to optimize this for us so that these lifted operator require no extra run-time costs.

> 好範例

### Breaking module boundary (datatype constrain)

...

This post is written as th second part for explaining
how to manipulate datatype with newtype and coerce.

# To overcome the shortcomings of `newtype`

we don't want to rewrite every instances

## Unsafe.Coerce

## GND

Here comes `GeneralizedNewtypeDeriving` (GND).
GND is part of GHC and design for generating instances for your newtype.
It will create some **coercions** at compile-time so that
instances for newtype can be build by the existing one.

> 好範例

GND-generated instances needs no run-time costs, however,
is not safe in every situation.
Since one can use GND to break invariant of datatype.

> 好範例

### Pre-GHC-7.8 time

GND was considered unsafe in Safe Haskell.

### Between GHC-7.8 to GHC-7.10

總結來說：7.8 之前 GND 有個很大的洞：
可以用 GND 來做出各種犯規的東西。[[見 Int-Age 例子](#)]
7.8 推出了 role system 去區別 type parameter，
然後用這個系統來定義 representational equality。
這提升了 GND 辨識與區別 data structures 的能力，
並且借此可以分辨哪些 instance 是 derivable 哪些不行。

此外也引入了 Data.Coerce。
讓 newtype 的使用有更完整的能力：更多 performance penalty 閃避法。

+ Data.Coerce 是從 Base 4.7.0.0 (GHC 7.8) 開始出現的。

### After GHC-7.10

## Role System and Data.Coerce

The `Data.Coerce` package defines `class Coercible a b` and `coerce :: Coercible * a b => a -> b`

# 參考資料

+ [Haskell 2010 Language Report](https://www.haskell.org/onlinereport/haskell2010/)
    + [3.12 Let Expressions](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-440003.12)
    + [3.17 Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-580003.17)
    + [3.17.3 Formal Semantics of Pattern Matching](https://www.haskell.org/onlinereport/haskell2010/haskellch3.html#x8-610003.17.3)
    + [4.2.3 Datatype Renamings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-740004.2.3)
    + [4.4.3.1 Function bindings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-840004.4.3.1)
    + [4.4.3.2 Pattern bindings](https://www.haskell.org/onlinereport/haskell2010/haskellch4.html#x10-860004.4.3.2)
